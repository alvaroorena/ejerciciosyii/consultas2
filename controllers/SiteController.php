<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
     public function actionConsulta1dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT COUNT(*) AS numciclistas FROM  ciclista c',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numciclistas'],
           "titulo"=>"Consulta 1 con DAO",
           "enunciado"=>"Número de ciclistas que hay",
           "sql"=>"SELECT COUNT(*) AS numciclistas FROM  ciclista c"
       ]);
        
        
    }
    
    
    public function actionConsulta1orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("COUNT(*) AS numciclistas ")
                ->distinct(),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numciclistas'],
           "titulo"=>"Consulta 1 con ORM",
           "enunciado"=>"Número de ciclistas que hay",
           "sql"=>"SELECT COUNT(*) AS numciclistas FROM  ciclista c"
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta2dao(){
        
      
        
       $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT COUNT(*) AS numciclistasbanesto FROM ciclista c WHERE c.nomequipo = "Banesto"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numciclistasbanesto'],
           "titulo"=>"Consulta 2 con DAO",
           "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
           "sql"=>'SELECT COUNT(*) AS numciclistasbanesto FROM ciclista c WHERE c.nomequipo = "Banesto"'
       ]);
        
        
    }
    
    public function actionConsulta2orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("COUNT(*) AS numciclistasbanesto ")
                ->distinct()
                ->where("nomequipo='Banesto'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numciclistasbanesto'],
           "titulo"=>"Consulta 2 con ORM",
           "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
           "sql"=>'SELECT COUNT(*) AS numciclistasbanesto FROM ciclista c WHERE c.nomequipo = "Banesto"'
       ]);
        
        
        
        
        
    }
    
    
     public function actionConsulta3dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT AVG(c.edad)AS edadmedia FROM ciclista c',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edadmedia'],
           "titulo"=>"Consulta 3 con DAO",
           "enunciado"=>"Edad media de los ciclistas",
           "sql"=>'SELECT AVG(c.edad) AS edadmedia  FROM ciclista c'
       ]);
        
        
    }
    
    public function actionConsulta3orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("AVG(edad) AS edadmedia")
                ->distinct(),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edadmedia'],
           "titulo"=>"Consulta 3 con ORM",
           "enunciado"=>"Edad media de los ciclistas",
           "sql"=>'SELECT AVG(c.edad) AS edadmedia  FROM ciclista c'
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta4dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT AVG(c.edad)AS edadmediabanesto FROM ciclista c WHERE nomequipo = "Banesto"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edadmediabanesto'],
           "titulo"=>"Consulta 4 con DAO",
           "enunciado"=>"La edad media de los del equipo Banesto",
           "sql"=>'SELECT AVG(c.edad)AS edadmediabanesto FROM ciclista c WHERE nomequipo = "Banesto"'
       ]);
        
        
    }
    
    public function actionConsulta4orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("AVG (edad) AS edadmediabanesto")
                ->where("nomequipo='Banesto'"),
                
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edadmediabanesto'],
           "titulo"=>"Consulta 4 con ORM",
           "enunciado"=>"La edad media de los del equipo Banesto",
           "sql"=>'SELECT AVG(c.edad)AS edadmediabanesto FROM ciclista c WHERE nomequipo = "Banesto"'
       ]);
        
        
        
        
    }
    
    
    public function actionConsulta5dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c.nomequipo, AVG(c.edad) AS "edadmediaporequipo" FROM ciclista c GROUP BY c.nomequipo',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo', 'edadmediaporequipo'],
           "titulo"=>"Consulta 5 con DAO",
           "enunciado"=>"La edad media de los ciclistas por cada equipo",
           "sql"=>' SELECT c.nomequipo, AVG(c.edad) AS edadmediaporequipo  FROM ciclista c GROUP BY c.nomequipo;
'
       ]);
        
        
    }
    
    public function actionConsulta5orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("nomequipo, AVG(edad) AS edadmediaporequipo ")
                ->groupBy("nomequipo"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo', 'edadmediaporequipo'],
           "titulo"=>"Consulta 5 con ORM",
           "enunciado"=>"La edad media de los ciclistas por cada equipo",
           "sql"=>'SELECT c.nomequipo, AVG(c.edad) AS edadmediaporequipo  FROM ciclista c GROUP BY c.nomequipo;
'
       ]);
        
        
        
        
    }
    
    
     public function actionConsulta6dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c.nomequipo, COUNT(*) AS numciclistasporequipo FROM ciclista c GROUP BY c.nomequipo',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo','numciclistasporequipo'],
           "titulo"=>"Consulta 6 con DAO",
           "enunciado"=>" El número de ciclistas por equipo",
           "sql"=>' SELECT c.nomequipo, COUNT(*) FROM ciclista c GROUP BY c.nomequipo;
'
       ]);
        
        
    }
    
    
    public function actionConsulta6orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("nomequipo, COUNT(*) AS numciclistasporequipo")
                ->groupBy('nomequipo'),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo','numciclistasporequipo'],
           "titulo"=>"Consulta 6 con ORM",
           "enunciado"=>" El número de ciclistas por equipo",
           "sql"=>'SELECT c.nomequipo, COUNT(*) AS numciclistasporequipo FROM ciclista c GROUP BY c.nomequipo;
'
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta7dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT COUNT(*)AS numpuertos FROM puerto',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numpuertos'],
           "titulo"=>"Consulta 7 con DAO",
           "enunciado"=>"El número total de puertos",
           "sql"=>' SELECT COUNT(*)AS numpuertos FROM puerto;
'
       ]);
        
        
    }
    
    public function actionConsulta7orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Puerto::find()
                ->select("COUNT(*) AS numpuertos"),
                
                   
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numpuertos'],
           "titulo"=>"Consulta 7 con ORM",
           "enunciado"=>" El número total de puertos",
           "sql"=>'SELECT COUNT(*)AS numpuertos FROM puerto;
'
       ]);
        
        
        
        
        
    }
    
    
    public function actionConsulta8dao(){
        
      
        
       $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT COUNT(*)AS numpuertosmayoresde1500 FROM puerto p WHERE p.altura >1500',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numpuertosmayoresde1500'],
           "titulo"=>"Consulta 8 con DAO",
           "enunciado"=>"El número total de puertos mayores de 1500",
           "sql"=>'SELECT COUNT(*)AS numpuertosmayoresde1500 FROM puerto p WHERE p.altura >1500;
'
       ]);
        
        
    }
    
    public function actionConsulta8orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Puerto::find()
                ->select("COUNT(*) AS numpuertosmayoresde1500")
                ->where('altura>1500'),
            
                
                   
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['numpuertosmayoresde1500'],
           "titulo"=>"Consulta 8 con ORM",
           "enunciado"=>"El número total de puertos mayores de 1500",
           "sql"=>'SELECT COUNT(*)AS numpuertosmayoresde1500 FROM puerto p WHERE p.altura >1500;
'
       ]);
        
        
        
        
        
    }
    
    
    
    public function actionConsulta9dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo'],
           "titulo"=>"Consulta 9 con DAO",
           "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
           "sql"=>'SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;
'
       ]);
        
        
    }
    
    public function actionConsulta9orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("nomequipo")
                ->groupBy("nomequipo")
                ->having("COUNT(*)>4"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo'],
           "titulo"=>"Consulta 9 con ORM",
           "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
           "sql"=>'SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;
'
       ]);
        
        
         
    }
    
    
    
    public function actionConsulta10dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo'],
           "titulo"=>"Consulta 10 con DAO",
           "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
           "sql"=>' SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4;
'
       ]);
        
        
    }
    
    public function actionConsulta10orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("nomequipo")
                ->where("edad BETWEEN 28 AND 32")
                ->groupBy("nomequipo")
                ->having("COUNT(*)>4"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
      return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nomequipo'],
           "titulo"=>"Consulta 10 con ORM",
           "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
           "sql"=>' SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4;
'
       ]);
        
        
         
    }
    
    public function actionConsulta11dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT COUNT(*) AS numvictoriasporetapa,e.dorsal FROM etapa e GROUP BY e.dorsal',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal','numvictoriasporetapa'],
           "titulo"=>"Consulta 11 con DAO",
           "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
           "sql"=>' SELECT COUNT(*) AS numvictoriasporetapa,e.dorsal FROM etapa e GROUP BY e.dorsal;
'
       ]);
        
        
    }
    
    public function actionConsulta11orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Etapa::find()
                ->select("COUNT(*) AS numvictoriasporetapa,dorsal")
                ->groupBy("dorsal"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
      return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal','numvictoriasporetapa'],
           "titulo"=>"Consulta 11 con ORM",
           "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
           "sql"=>'SELECT COUNT(*) AS numvictoriasporetapa,e.dorsal FROM etapa e GROUP BY e.dorsal;
'
       ]);
        
        
         
    }
    
    public function actionConsulta12dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT e.dorsal  FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 12 con DAO",
           "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
           "sql"=>' SELECT e.dorsal  FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;
'
       ]);
        
        
    }
    
    public function actionConsulta12orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Etapa::find()
                ->select("dorsal")
                ->groupBy("dorsal")
                ->having("COUNT(*)>1"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
      return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 12 con ORM",
           "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
           "sql"=>'SELECT e.dorsal  FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;
'
       ]);
        
        
         
    }
    
    
    
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
